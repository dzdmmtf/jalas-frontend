FROM node:12
WORKDIR /code
COPY jalas/package.json /code/package.json
RUN npm install --silent
RUN npm install react-scripts@3.0.1 -g --silent
COPY jalas .
CMD ["npm", "start"]