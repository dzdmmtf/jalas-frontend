import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import * as serviceWorker from './serviceWorker';
import Home from './components/home/Home';
import PollsList from './components/poll/PollsList';
import MeetingsList from './components/meeting/MeetingsList';
import PollInfo from './components/poll/PollInfo';
import CreatePoll from "./components/poll/CreatePoll";
import MeetingInfo from './components/meeting/MeetingInfo';
import CommentView from './components/poll/Comment';
import LoginView from './components/authentication/Login';
import NotificationView from './components/notification/Notification';
import {BrowserRouter as Router, Route} from 'react-router-dom'
import 'bootstrap/dist/css/bootstrap.min.css';
import {Switch} from "react-router";

ReactDOM.render(
    <Router>
    <Switch>
        <Route exact path="/" component={Home}/>
        <Route exact path="/poll" component={PollsList}/>
        <Route exact path="/meeting" component={MeetingsList}/>
        <Route exact path="/poll/create" component={CreatePoll}/>
        <Route exact path="/poll/:pollId" component={PollInfo}/>

        <Route exact path="/meeting/:meetingId" component={MeetingInfo}/>
        <Route exact path="/poll/:pollId/comment" component={CommentView}/>
        <Route exact path="/auth/token" component={LoginView}/>
        <Route exact path="/notification" component={NotificationView}/>
    </Switch>
    </Router>,
    document.getElementById(`root`)
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();


