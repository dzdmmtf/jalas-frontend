import React, {Component} from 'react'
import {Button} from 'react-bootstrap';
import Navbar from "react-bootstrap/Navbar";
import './Home.css'

export default class Home extends Component {
    render() {
        console.log(process.env)
        return (
            <div>
                <Navbar bg="dark" variant="dark">
                    <Navbar.Text className="nav-location"><h1> Jalas System </h1></Navbar.Text>
                </Navbar>
                <div className="button-div">
                    <Button href="/poll" variant="outline-dark"> <b> Polls </b>  </Button>
                </div>
                <div className="button-div">
                    <Button href="/meeting" variant="outline-dark"> <b> Meetings </b>  </Button>
                </div>
                <div className="button-div">
                    <Button href="/notification" variant="outline-dark"> <b> Arrange Notifications </b> </Button>
                </div>
            </div>
        )
    }
}
