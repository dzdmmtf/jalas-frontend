import React, {Component} from 'react'
import {Redirect} from 'react-router'
import './Notification.css'
import axios from 'axios';
import Navbar from "react-bootstrap/Navbar";
import {Button} from 'react-bootstrap';
import Auth from "../authentication/Auth";
import Checkbox from '@material-ui/core/Checkbox';
import FormControlLabel from "@material-ui/core/FormControlLabel";
import FormGroup from "react-bootstrap/FormGroup";
import CheckBoxOutlineBlankIcon from '@material-ui/icons/CheckBoxOutlineBlank';
import CheckBoxIcon from '@material-ui/icons/CheckBox';

export default class Notification extends Component {
    constructor(props) {
        super(props);
        this.state = {
            options: [
                "reserve_meeting",
                "create_poll",
                "submit_vote",
                "add_poll_option",
                "delete_poll_option",
                "add_participant",
                "remove_participant"
            ],
            checkedValues: []
        }
    }

    componentDidMount() {
        let setState = this.setState.bind(this);

        axios.get("http://" + process.env.REACT_APP_BACKEND_HOST + ":" + process.env.REACT_APP_BACKEND_PORT + "/account/notification/",
            { headers: { Authorization: `Bearer ${Auth.getAccessToken()}`}}).then(response => {
                let values = Object.values(response.data);
                let keys = Object.keys(response.data);
                let checkedValues = [];
                values.forEach((value, index) => {
                    if (value === true) {
                        checkedValues.push(keys[index])
                    }
                });

            setState({checkedValues});
        }).catch(error => {
            if (error.response && error.response.data.code === "token_not_valid") {
                axios.post("http://" + process.env.REACT_APP_BACKEND_HOST + ":" + process.env.REACT_APP_BACKEND_PORT + "/auth/token/refresh/", {
                    refresh: Auth.getRefreshToken()
                }).then(response => {
                    Auth.authenticateUser(response.data.access, Auth.getRefreshToken());
                    axios.get("http://" + process.env.REACT_APP_BACKEND_HOST + ":" + process.env.REACT_APP_BACKEND_PORT + "/account/notification/",
                        { headers: { Authorization: `Bearer ${Auth.getAccessToken()}`}}).then(response => {
                        let values = Object.values(response.data);
                        let keys = Object.keys(response.data);
                        let checkedValues = [];
                        values.forEach((value, index) => {
                            if (value === true) {
                                checkedValues.push(keys[index])
                            }
                        });
                        setState({checkedValues});
                    }).catch(err => {
                        console.log(err);
                    })
                })
            } else if (error.response && error.response.status === 403) {
                alert('you are not allowed to get notification arrangement')
            }  else {
                console.log(error);
            }
        })
    }

    handleChange(option, value) {
        this.setState(state => ({
            checkedValues: state.checkedValues.includes(option)
                ? state.checkedValues.filter(c => c !== option)
                : [...state.checkedValues, option]
        }));
    }

    arrangeNotifications = () => {
        let {checkedValues} = this.state;
        axios.post("http://" + process.env.REACT_APP_BACKEND_HOST + ":" + process.env.REACT_APP_BACKEND_PORT + "/account/notification/", {
            "reserve_meeting": checkedValues.includes('reserve_meeting'),
            "create_poll": checkedValues.includes('create_poll'),
            "submit_vote": checkedValues.includes('submit_vote'),
            "add_poll_option": checkedValues.includes('add_poll_option'),
            "delete_poll_option": checkedValues.includes('delete_poll_option'),
            "add_participant": checkedValues.includes('add_participant'),
            "remove_participant": checkedValues.includes('remove_participant')
        },{headers: {Authorization: `Bearer ${Auth.getAccessToken()}`}
        }).then(response => {
            this.setState({
                redirect: true
            });
        }).catch(error => {
            if (error.response && error.response.data.code === "token_not_valid") {
                axios.post("http://" + process.env.REACT_APP_BACKEND_HOST + ":" + process.env.REACT_APP_BACKEND_PORT + "/auth/token/refresh/", {
                    refresh: Auth.getRefreshToken()
                }).then(response => {
                    Auth.authenticateUser(response.data.access, Auth.getRefreshToken());
                    axios.post("http://" + process.env.REACT_APP_BACKEND_HOST + ":" + process.env.REACT_APP_BACKEND_PORT + "/account/notification/", {
                        "reserve_meeting": checkedValues.includes('reserve_meeting'),
                        "create_poll": checkedValues.includes('create_poll'),
                        "submit_vote": checkedValues.includes('submit_vote'),
                        "add_poll_option": checkedValues.includes('add_poll_option'),
                        "delete_poll_option": checkedValues.includes('delete_poll_option'),
                        "add_participant": checkedValues.includes('add_participant'),
                        "remove_participant": checkedValues.includes('remove_participant')
                    }, {headers: {Authorization: `Bearer ${Auth.getAccessToken()}`}
                    }).then(response => {
                        this.setState({
                            redirect: true
                        });
                    }).catch(err => {
                        console.log(err);
                    })
                })
            } else if (error.response && error.response.status === 403) {
                alert('you are not allowed to arrange notifications')
            } else {
                console.log(error);
            }
        })
    };


    render() {
        if (this.state.redirect)
            return <Redirect to={"/"}/>;

        return (
            <div>
                <Navbar bg="dark" variant="dark">
                    <Navbar.Text className="nav-location"><h1> Jalas System </h1></Navbar.Text>
                </Navbar>
                <h4 className="title"><b> Choose Desired Items for Sending Notification </b></h4>
                <div className="box-location" style={{width:"200px",height:"450px",border:"1px solid" }}>
                    {this.state.options.map(option => (
                        <div>
                            <FormGroup row className="checkbox">
                                <FormControlLabel
                                    control={
                                        <Checkbox
                                            icon={<CheckBoxOutlineBlankIcon fontSize="small"/>}
                                            checkedIcon={<CheckBoxIcon fontSize="small"/>}
                                            checked={this.state.checkedValues.includes(option)}
                                            color="black"
                                            onChange={e => this.handleChange(option, e)}
                                        />
                                    }
                                    label={option}
                                />
                            </FormGroup>
                        </div>

                    ))}
                </div>

                <Button onClick={this.arrangeNotifications} variant="outline-dark" className="save-location">
                    <b> Save </b>
                </Button>

                <Button onClick={() => {this.setState({redirect: true})}} variant="outline-dark" className="back-location">
                    <b> Back </b>
                </Button>

            </div>
        )
    }
}