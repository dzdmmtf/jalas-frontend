import React, { Component } from 'react'
import axios from 'axios';
import Navbar from "react-bootstrap/Navbar";
import {Button} from "react-bootstrap";
import './Meeting.css';
import Auth from "../authentication/Auth";

export default class MeetingsList extends Component {
    componentDidMount() {
        let setState = this.setState.bind(this);

        axios.get("http://" + process.env.REACT_APP_BACKEND_HOST + ":" + process.env.REACT_APP_BACKEND_PORT + "/meeting/",
            { headers: { Authorization: `Bearer ${Auth.getAccessToken()}`}}).then( response => {
            setState({meetings: response.data.meetings});
        }).catch( error => {
            if (error.response && error.response.data.code === "token_not_valid") {
                axios.post("http://" + process.env.REACT_APP_BACKEND_HOST + ":" + process.env.REACT_APP_BACKEND_PORT + "/auth/token/refresh/", {
                    refresh: Auth.getRefreshToken()
                }).then(response => {
                    Auth.authenticateUser(response.data.access, Auth.getRefreshToken());
                    axios.get("http://" + process.env.REACT_APP_BACKEND_HOST + ":" + process.env.REACT_APP_BACKEND_PORT + "/meeting/",
                        { headers: { Authorization: `Bearer ${Auth.getAccessToken()}`}}).then(response => {
                        setState({meetings: response.data.meetings});
                    }).catch(err => {
                        console.log(err);
                    })
                })
            } else if (error.response && error.response.status === 403) {
                alert('you are not allowed to see meeting list')
            } else {
                console.log(error);
            }
        })
    }
    
    render() {
        if(!this.state)
            return (<div></div>);

        const meetingsJSX = this.state.meetings.map(m => {
            const meeting = JSON.parse(JSON.stringify(m));
            return (

                <div className="button-div p-3 flex-fill bd-highlight col-example">
                    <Button variant="outline-dark" href={"/meeting/" + meeting.id + "/"} key={meeting.id} id={meeting.id}>
                        <div>
                            <b>title: {meeting.title}</b>
                        </div>
                        <div>
                            <b>room_number: {meeting.room_number}</b>
                        </div>
                        <div>
                            <b>start: {meeting.start}</b>
                        </div>
                        <div>
                            <b>end: {meeting.end}</b>
                        </div>
                        <div>
                            <b>status: {meeting.status}</b>
                        </div>
                    </Button>
                </div>
            )
        });

        return (
            <div>
                <Navbar bg="dark" variant="dark">
                    <Navbar.Text className="nav-location"><h1> Jalas System </h1></Navbar.Text>
                </Navbar>

                <div id='meetings-container' className="d-flex bd-highlight example-parent someNew">
                    {meetingsJSX}
                </div>
            </div>
        )
    }
}
