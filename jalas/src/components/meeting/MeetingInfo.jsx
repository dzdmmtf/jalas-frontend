import React, { Component } from 'react'
import axios from 'axios'
import { Redirect } from 'react-router'
import Auth from "../authentication/Auth";
import Navbar from "react-bootstrap/Navbar";


export default class MeetingInfo extends Component {
    componentDidMount() {
        let setState = this.setState.bind(this);

        axios.get("http://" + process.env.REACT_APP_BACKEND_HOST + ":" + process.env.REACT_APP_BACKEND_PORT + "/meeting/" + this.props.match.params.meetingId + "/",
            { headers: { Authorization: `Bearer ${Auth.getAccessToken()}`}}).then( response => {
            setState({meeting: response.data});
        }).catch( error => {
            if (error.response && error.response.data.code === "token_not_valid") {
                axios.post("http://" + process.env.REACT_APP_BACKEND_HOST + ":" + process.env.REACT_APP_BACKEND_PORT + "/auth/token/refresh/", {
                    refresh: Auth.getRefreshToken()
                }).then(response => {
                    Auth.authenticateUser(response.data.access, Auth.getRefreshToken());
                    axios.get("http://" + process.env.REACT_APP_BACKEND_HOST + ":" + process.env.REACT_APP_BACKEND_PORT + "/meeting/" + this.props.match.params.meetingId + "/",
                        { headers: { Authorization: `Bearer ${Auth.getAccessToken()}`}}).then(response => {
                        setState({meeting: response.data});
                    }).catch(err => {
                        console.log(err);
                    })
                })
            } else if (error.response && error.response.status === 403) {
                alert('you are not allowed to see this meeting')
            } else if (error.response && error.response.status === 404) {
                alert('there is no such a meeting with this id')
            } else {
                console.log(error);
            }
        })
    }

    cancel = () => {
        const {meeting} = this.state;
        let forceUpdate = this.forceUpdate.bind(this);
        axios.put("http://" + process.env.REACT_APP_BACKEND_HOST + ":" + process.env.REACT_APP_BACKEND_PORT + "/meeting/" + this.props.match.params.meetingId + "/", {
            "action": "cancel"
        }, { headers: { Authorization: `Bearer ${Auth.getAccessToken()}`}}).then( response => {
            meeting.status = 'canceled';
            forceUpdate()
        }).catch( error => {
            if (error.response && error.response.data.code === "token_not_valid") {
                axios.post("http://" + process.env.REACT_APP_BACKEND_HOST + ":" + process.env.REACT_APP_BACKEND_PORT + "/auth/token/refresh/", {
                    refresh: Auth.getRefreshToken()
                }).then(response => {
                    Auth.authenticateUser(response.data.access, Auth.getRefreshToken());
                    axios.put("http://" + process.env.REACT_APP_BACKEND_HOST + ":" + process.env.REACT_APP_BACKEND_PORT + "/meeting/" + this.props.match.params.meetingId + "/", {
                        "action": "cancel"
                    },  { headers: { Authorization: `Bearer ${Auth.getAccessToken()}`}}).then(response => {
                        meeting.status = 'canceled';
                        forceUpdate()
                    }).catch(err => {
                        console.log(err);
                    })
                })
            } else if (error.response && error.response.status === 403) {
                alert('you are not allowed to cancel this meeting')
            } else if (error.response && error.response.status === 404) {
                alert('there is no such a meeting with this id')
            } else {
                console.log(error);
            }
        })
    };

    delete = () => {
        let setState = this.setState.bind(this);
        axios.delete("http://" + process.env.REACT_APP_BACKEND_HOST + ":" + process.env.REACT_APP_BACKEND_PORT + "/meeting/" + this.props.match.params.meetingId + "/",
            { headers: { Authorization: `Bearer ${Auth.getAccessToken()}`}}).then( response => {
            setState({
                redirect: true,
            })
        }).catch( error => {
            if (error.response && error.response.data.code === "token_not_valid") {
                axios.post("http://" + process.env.REACT_APP_BACKEND_HOST + ":" + process.env.REACT_APP_BACKEND_PORT + "/auth/token/refresh/", {
                    refresh: Auth.getRefreshToken()
                }).then(response => {
                    Auth.authenticateUser(response.data.access, Auth.getRefreshToken());
                    axios.delete("http://" + process.env.REACT_APP_BACKEND_HOST + ":" + process.env.REACT_APP_BACKEND_PORT + "/meeting/" + this.props.match.params.meetingId + "/",
                        { headers: { Authorization: `Bearer ${Auth.getAccessToken()}`}}).then(response => {
                        setState({
                            redirect: true,
                        })
                    }).catch(err => {
                        console.log(err);
                    })
                })
            } else if (error.response && error.response.status === 403) {
                alert('you are not allowed to delete this meeting')
            } else if (error.response && error.response.status === 404) {
                alert('there is no such a meeting with this id')
            } else {
                console.log(error);
            }
        })
    };

    render() {
        if(!this.state)
            return (<div></div>);

        if(this.state.redirect){
            return <Redirect to={"/meeting"}/>
        }

        const {meeting} = this.state;
        const inviteesJSX = meeting.invitees.map(i => {
            return (
                <li>
                    <p><b>Email: </b> {i.email}</p>
                </li>
            )
        });

        const cancelButton = (meeting.status === 'not registered') ? <button onClick={this.cancel}>cancel</button>:null;

        return (
            <div>
                <Navbar bg="dark" variant="dark">
                    <Navbar.Text className="nav-location"><h1> Jalas System </h1></Navbar.Text>
                </Navbar>

                <div id='meeting-info-container' className="meeting-info">
                    <p><b>Title: </b> {meeting.title}</p>
                    <p><b>Creator: </b> {meeting.creator}</p>
                    <p><b>Room Number: </b> {meeting.room_number}</p>
                    <p><b>Start: </b> {meeting.start}</p>
                    <p><b>End: </b> {meeting.end}</p>
                    <p><b>Status: </b> {meeting.status}</p>
                    <p>
                        <b>Invitees: </b>
                        <ul>{inviteesJSX}</ul>
                    </p>
                    {cancelButton}
                    <button onClick={this.delete}>delete</button>
                </div>
            </div>
        )
    }
}
