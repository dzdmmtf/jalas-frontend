import React, {Component} from 'react'
import '../home/Home.css'
import axios from 'axios';
import Navbar from "react-bootstrap/Navbar";
import {Button} from 'react-bootstrap';
import Form from "react-bootstrap/Form";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import {Redirect} from 'react-router';
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import Auth from "../authentication/Auth";



function validate(title, invitees) {
    const errors = {email: [], title: []};

    if (!title) {
        errors.title.push("Title can't be empty");
    }

    if (invitees[0] === "") {
        errors.email.push("Select at least one invitee")
    }

    invitees.forEach(email => {
        if (email.length < 5) {
            errors.email.push("Email should be at least 5 characters long");
        }
        if (email.split("").filter(x => x === "@").length !== 1) {
            errors.email.push("Email should contain a @");
        }
        if (email.indexOf(".") === -1) {
            errors.email.push("Email should contain at least one dot");
        }
    });

    return errors;
}


export default class CreatePoll extends Component {
    state = {
        title: '',
        invitees: [''],
        startDate: [],
        endDate: [],
        errors: {email: [], title: []}
    };

    handleTitleChange = i => e => {
        let title = e.target.value;
        this.setState({
            title
        })
    };

    handleInviteesChange = i => e => {
        let invitees = [...this.state.invitees];
        invitees[i] = e.target.value;
        this.setState({
            invitees
        })
    };

    handleStartOptionChange(i, date) {
        let startDate = [...this.state.startDate];
        startDate[i] = date;

        this.setState({
           startDate
        })
    };

    handleEndOptionChange(i, date) {
        let endDate = [...this.state.endDate];
        endDate[i] = date;

        this.setState({
            endDate
        })
    };

    addInvitee = e => {
        e.preventDefault();
        let invitees = this.state.invitees.concat(['']);
        this.setState({
            invitees
        })
    };

    addOption = e => {
        e.preventDefault();
        let startDate = this.state.startDate.concat(['']);
        let endDate = this.state.endDate.concat(['']);
        this.setState({
            startDate
        });
        this.setState({
            endDate
        });
    };

    createPoll = () => {
        let setState = this.setState.bind(this);
        const {title, invitees} = this.state;
        const errors = validate(title, invitees);
        if (errors.email.length !== 0 || errors.title.length !== 0) {
            this.setState({errors});
        } else {
            this.setState({
                errors: {email: [], title: []}
            });
            let options = [];
            this.state.startDate.forEach((startDate, i) => {
                let tmp = [this.state.startDate[i], this.state.endDate[i]];
                options.push(tmp);
            });
            axios.post("http://" + process.env.REACT_APP_BACKEND_HOST + ":" + process.env.REACT_APP_BACKEND_PORT + "/poll/", {
                "title": title,
                "invitees": invitees,
                "startDate": this.state.startDate,
                "endDate": this.state.endDate
            }, { headers: { Authorization: `Bearer ${Auth.getAccessToken()}`}}).then(response => {
                setState({
                    redirect: true
                })
            }).catch(error => {
                if (error.response && error.response.data.code === "token_not_valid") {
                    axios.post('http://' + process.env.REACT_APP_BACKEND_HOST + ':' + process.env.REACT_APP_BACKEND_PORT + '/auth/token/refresh/', {
                        refresh: Auth.getRefreshToken()
                    }).then(response => {
                        Auth.authenticateUser(response.data.access, Auth.getRefreshToken());
                        axios.post("http://" + process.env.REACT_APP_BACKEND_HOST + ":" + process.env.REACT_APP_BACKEND_PORT + "/poll/", {
                            "title": title,
                            "invitees": invitees,
                            "startDate": this.state.startDate,
                            "endDate": this.state.endDate
                        },  { headers: { Authorization: `Bearer ${Auth.getAccessToken()}`}}).then(response => {
                            setState({
                                redirect: true
                            })
                        }).catch(err => {
                            console.log(err);
                        })
                    })
                } else if (error.response && error.response.status === 403) {
                    alert('you are not allowed to create poll')
                } else if (error.response && error.response.status === 400) {
                    alert('at least one of the invitees is not registered in jalas system')
                } else if (error.response && error.response.status === 500) {
                    alert('maybe your internet connection is poor')
                } else {
                    console.log(error);
                }
            })
        }
    };


    render() {
        if (!this.state)
            return (<div></div>);

        if (this.state.redirect)
            return <Redirect to={"/poll/"}/>;

        const newSet = new Set(this.state.errors.email);
        const emailErrors = [...newSet];
        return (
            <div>
                <Navbar bg="dark" variant="dark">
                    <Navbar.Text className="nav-location"><h1> Jalas System </h1></Navbar.Text>
                </Navbar>

                <Form.Group as={Row}>
                    <Form.Label className="title-label-location">
                        Title
                    </Form.Label>
                    <Form.Control className="title-placeholder-location" placeholder="Title"
                                  onChange={this.handleTitleChange()}/>
                </Form.Group>

                {this.state.errors.title.map(error => (
                    <p className="show-errors"> <b> {error} </b> </p>
                ))}

                {this.state.invitees.map((invitee, index) => (
                    <div key={index}>
                        <Form.Group as={Row} controlId="formHorizontalEmail">
                            <Form.Label className="invitee-label-location">
                                Invitee
                            </Form.Label>
                            <Form.Control className="invitee-placeholder-location" type="email" placeholder="Invitee"
                                          onChange={this.handleInviteesChange(index)}/>
                        </Form.Group>
                    </div>
                ))}

                {emailErrors.map(error => (
                    <p className="show-errors"> <b> {error} </b> </p>
                ))}
                <div>
                    <Button variant="outline-dark" className="add-invitee-location" onClick={this.addInvitee}>Add New Invitee</Button>
                </div>

                {this.state.startDate.map((option, index) => (
                    <div>
                        <div className="option-location rowC">
                            <p>Start</p>
                            <div className="datetime-location">
                                <DatePicker
                                    selected={(this.state.startDate.length === 0) ? new Date() :
                                        (this.state.startDate[0] === "") ? new Date() : this.state.startDate[index]}
                                    onChange={(date) => this.handleStartOptionChange(index, date)}
                                    showTimeSelect
                                    timeFormat="HH:mm"
                                    timeIntervals={15}
                                    timeCaption="time"
                                    dateFormat="MMMM d, yyyy h:mm aa"
                                />
                            </div>
                        </div>
                        <div className="option-location rowC">
                            <p>End</p>
                            <div className="datetime-location">
                                <DatePicker
                                    selected={(this.state.endDate.length === 0) ? new Date() :
                                        (this.state.endDate[0] === "") ? new Date() : this.state.endDate[index]}
                                    onChange={(date) => this.handleEndOptionChange(index, date)}
                                    showTimeSelect
                                    timeFormat="HH:mm"
                                    timeIntervals={15}
                                    timeCaption="time"
                                    dateFormat="MMMM d, yyyy h:mm aa"
                                />
                            </div>
                        </div>
                    </div>
                ))}

                <div>
                    <Button variant="outline-dark" className="add-option-location" onClick={this.addOption}>Add New Option</Button>
                </div>

                <Form.Group as={Row}>
                    <Col>
                        <Button variant="outline-dark" className="submit-create-poll-location" type="submit" onClick={this.createPoll}>Save
                            Changes</Button>
                    </Col>
                </Form.Group>

            </div>
        )
    }
}