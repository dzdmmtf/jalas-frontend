import React, {Component} from 'react'
import {Redirect} from 'react-router'
import './Poll.css'
import axios from 'axios';
import Navbar from "react-bootstrap/Navbar";
import {Button} from 'react-bootstrap';
import Row from "react-bootstrap/Row";
import Form from "react-bootstrap/Form";
import Auth from "../authentication/Auth";
import DatePicker from "react-datepicker";


function validate(invitees) {
    const errors = {email: []};

    invitees.forEach(email => {
        if (email.length < 5) {
            errors.email.push("Email should be at least 5 characters long");
        }
        if (email.split("").filter(x => x === "@").length !== 1) {
            errors.email.push("Email should contain a @");
        }
        if (email.indexOf(".") === -1) {
            errors.email.push("Email should contain at least one dot");
        }
    });

    return errors;
}

export default class PollInfo extends Component {
    state = {
        startDate: [],
        endDate: [],
        selectedOption: {
            id: "",
            index: ""
        },
        availableRooms: "",
        redirect: false,
        meetingId: "",
        createMeeting: "",
        roomNumber: "",
        refreshPage: false,
        comment: "",
        creationTimeStart: '',
        creationTimeEnd: '',
        errors: [],
        addedInvitees: []
    };

    componentDidMount() {

        let setState = this.setState.bind(this);
        axios.get('http://' + process.env.REACT_APP_BACKEND_HOST + ':' + process.env.REACT_APP_BACKEND_PORT + '/poll/' + this.props.match.params.pollId + "/",
            {headers: {Authorization: `Bearer ${Auth.getAccessToken()}`}}).then(response => {
            setState({poll: response.data});
        }).catch(error => {
            if (error.response && error.response.data.code === "token_not_valid") {
                axios.post('http://' + process.env.REACT_APP_BACKEND_HOST + ':' + process.env.REACT_APP_BACKEND_PORT + '/auth/token/refresh/', {
                    refresh: Auth.getRefreshToken()
                }).then(response => {
                    Auth.authenticateUser(response.data.access, Auth.getRefreshToken());
                    axios.get('http://' + process.env.REACT_APP_BACKEND_HOST + ':' + process.env.REACT_APP_BACKEND_PORT + '/poll/' + this.props.match.params.pollId + "/",
                        {headers: {Authorization: `Bearer ${Auth.getAccessToken()}`}}).then(response => {
                        setState({poll: response.data});
                    }).catch(err => {
                        console.log(err);
                    })
                })
            } else if (error.response && error.response.status === 403) {
                alert('you are not allowed to see this poll')
            } else if (error.response && error.response.status === 404) {
                alert('there is no such a poll with this id')
            } else {
                console.log(error.response);
            }
        })
    }

    getAvailableRooms = (optionId, index) => {
        let setState = this.setState.bind(this);
        setState({
            selectedOption: {id: optionId, index: index}
        });

        axios.get("http://" + process.env.REACT_APP_BACKEND_HOST + ":" + process.env.REACT_APP_BACKEND_PORT + "/poll/" + this.props.match.params.pollId + "/" + optionId + "/available_rooms/",
            {headers: {Authorization: `Bearer ${Auth.getAccessToken()}`}}).then(response => {
            setState({
                availableRooms: response.data.availableRooms.join()
            });
        }).catch(error => {
            if (error.response && error.response.data.code === "token_not_valid") {
                axios.post('http://' + process.env.REACT_APP_BACKEND_HOST + ':' + process.env.REACT_APP_BACKEND_PORT + '/auth/token/refresh/', {
                    refresh: Auth.getRefreshToken()
                }).then(response => {
                    Auth.authenticateUser(response.data.access, Auth.getRefreshToken());
                    axios.get('http://' + process.env.REACT_APP_BACKEND_HOST + ':' + process.env.REACT_APP_BACKEND_PORT + '/poll/' + this.props.match.params.pollId + "/" + optionId + "/available_rooms/",
                        {headers: {Authorization: `Bearer ${Auth.getAccessToken()}`}}).then(response => {
                        setState({
                            availableRooms: response.data.availableRooms.join()
                        });
                    }).catch(err => {
                        console.log(err);
                        setState({
                            availableRooms: 'Something Bad Happened in Getting Available Rooms'
                        })
                    })
                })
            } else if (error.response && error.response.status === 403) {
                alert('you are not allowed to get available rooms of this poll option')
            } else {
                console.log(error);
                setState({
                    availableRooms: 'Something Bad Happened in Getting Available Rooms'
                })
            }

        })
    };

    createMeeting = () => {
        let setState = this.setState.bind(this);

        const {selectedOption} = this.state;
        const {roomNumber} = this.state;

        if (!selectedOption) {
            alert("You should select a poll option");
            return
        }

        if (!roomNumber) {
            alert("You should select a room");
            return
        }

        axios.post("http://" + process.env.REACT_APP_BACKEND_HOST + ":" + process.env.REACT_APP_BACKEND_PORT + "/meeting/", {
            "room_number": roomNumber,
            "poll_option_id": selectedOption.id,
        }, {headers: {Authorization: `Bearer ${Auth.getAccessToken()}`}}).then(response => {
            setState({
                redirect: true,
                meetingId: response.data.meeting_id
            })
        }).catch(error => {
            if (error.response && error.response.data.code === "token_not_valid") {
                axios.post('http://' + process.env.REACT_APP_BACKEND_HOST + ':' + process.env.REACT_APP_BACKEND_PORT + '/auth/token/refresh/', {
                    refresh: Auth.getRefreshToken()
                }).then(response => {
                    Auth.authenticateUser(response.data.access, Auth.getRefreshToken());
                    axios.post("http://" + process.env.REACT_APP_BACKEND_HOST + ":" + process.env.REACT_APP_BACKEND_PORT + "/meeting/", {
                        "room_number": roomNumber,
                        "poll_option_id": selectedOption.id,
                    }, {headers: {Authorization: `Bearer ${Auth.getAccessToken()}`}}).then(response => {
                        setState({
                            redirect: true,
                            meetingId: response.data.meeting_id
                        })
                    }).catch(err => {
                        console.log(err);
                    })
                })
            } else if (error.response && error.response.status === 403) {
                alert('you are not allowed to create meeting')
            } else {
                console.log(error);
                if (error.response && error.response.status === 400) {
                    setState({
                        createMeeting: 'Room already reserved'
                    });
                }
                if (error.response && error.response.status === 404) {
                    setState({
                        createMeeting: 'Invalid Room number'
                    });
                }
                if (error.response && error.response.status === 503) {
                    setState({
                        createMeeting: 'Service temporary unavailable'
                    });
                }
                if (error.response && error.response.status === 500) {
                    setState({
                        createMeeting: 'Something Bad Happened in Create Meeting'
                    });
                }
            }
        })
    };

    updateRoomNumber(e) {
        this.setState({
            roomNumber: e.target.value
        })
    }

    handleVotes(optionId, vote) {
        let setState = this.setState.bind(this);
        axios.post("http://" + process.env.REACT_APP_BACKEND_HOST + ":" + process.env.REACT_APP_BACKEND_PORT + "/poll/" + this.props.match.params.pollId + "/" + optionId + "/vote/", {
            "value": vote
        }, {headers: {Authorization: `Bearer ${Auth.getAccessToken()}`}}).then(response => {
            setState({
                refreshPage: true
            });
        }).catch(error => {
            if (error.response && error.response.data.code === "token_not_valid") {
                axios.post('http://' + process.env.REACT_APP_BACKEND_HOST + ':' + process.env.REACT_APP_BACKEND_PORT + '/auth/token/refresh/', {
                    refresh: Auth.getRefreshToken()
                }).then(response => {
                    Auth.authenticateUser(response.data.access, Auth.getRefreshToken());
                    axios.post("http://" + process.env.REACT_APP_BACKEND_HOST + ":" + process.env.REACT_APP_BACKEND_PORT + "/poll/" + this.props.match.params.pollId + "/" + optionId + "/vote/", {
                        "value": vote
                    }, {headers: {Authorization: `Bearer ${Auth.getAccessToken()}`}}).then(response => {
                        setState({
                            refreshPage: true
                        });
                    }).catch(err => {
                        console.log(err);
                    })
                })
            } else if (error.response && error.response.status === 403) {
                alert('you are not allowed to vote')
            } else if (error.response && error.response.status === 400) {
                alert('you have already voted to this poll option')
            } else if (error.response && error.response.status === 500) {
                alert('maybe your internet connection is poor')
            } else {
                console.log(error);
            }
        })
    }

    handleStartOptionChange(i, date) {
        let startDate = [...this.state.startDate];
        startDate[i] = date;

        this.setState({
            startDate
        })
    };

    handleEndOptionChange(i, date) {
        let endDate = [...this.state.endDate];
        endDate[i] = date;

        this.setState({
            endDate
        })
    };

    addOption = e => {
        e.preventDefault();
        let startDate = this.state.startDate.concat(['']);
        let endDate = this.state.endDate.concat(['']);
        this.setState({
            startDate
        });
        this.setState({
            endDate
        });
    };

    handleComment(c) {
        this.setState({
            comment: c.target.value
        });
    }

    postComment = () => {
        let {comment} = this.state;
        if (!comment) {
            alert('please enter a comment');
            return
        }
        axios.post("http://" + process.env.REACT_APP_BACKEND_HOST + ":" + process.env.REACT_APP_BACKEND_PORT + "/poll/" + this.props.match.params.pollId + "/comment/", {
            "text": this.state.comment
        }, {headers: {Authorization: `Bearer ${Auth.getAccessToken()}`}}).then(response => {
            this.setState({
                comment: ""
            })
        }).catch(error => {
            if (error.response && error.response.data.code === "token_not_valid") {
                axios.post('http://' + process.env.REACT_APP_BACKEND_HOST + ':' + process.env.REACT_APP_BACKEND_PORT + '/auth/token/refresh/', {
                    refresh: Auth.getRefreshToken()
                }).then(response => {
                    Auth.authenticateUser(response.data.access, Auth.getRefreshToken());
                    axios.post("http://" + process.env.REACT_APP_BACKEND_HOST + ":" + process.env.REACT_APP_BACKEND_PORT + "/poll/" + this.props.match.params.pollId + "/comment/", {
                        "text": this.state.comment
                    }, {headers: {Authorization: `Bearer ${Auth.getAccessToken()}`}}).then(response => {
                        this.setState({
                            comment: ""
                        })
                    }).catch(err => {
                        console.log(err);
                        this.setState({
                            comment: ""
                        })
                    })
                })
            } else if (error.response && error.response.status === 403) {
                alert('you are not allowed to comment on this poll');
                this.setState({
                    comment: ""
                })
            } else {
                console.log(error);
                this.setState({
                    comment: ""
                })
            }
        })
    };

    addOptionToPoll = () => {
        axios.post("http://" + process.env.REACT_APP_BACKEND_HOST + ":" + process.env.REACT_APP_BACKEND_PORT + "/poll/" + this.props.match.params.pollId + "/option/", {
            "startDate": this.state.startDate,
            "endDate": this.state.endDate
        }, {headers: {Authorization: `Bearer ${Auth.getAccessToken()}`}}).then(response => {
            this.setState({
                refreshPage: true
            });
        }).catch(error => {
            if (error.response && error.response.data.code === "token_not_valid") {
                axios.post('http://' + process.env.REACT_APP_BACKEND_HOST + ':' + process.env.REACT_APP_BACKEND_PORT + '/auth/token/refresh/', {
                    refresh: Auth.getRefreshToken()
                }).then(response => {
                    Auth.authenticateUser(response.data.access, Auth.getRefreshToken());
                    axios.post("http://" + process.env.REACT_APP_BACKEND_HOST + ":" + process.env.REACT_APP_BACKEND_PORT + "/poll/" + this.props.match.params.pollId + "/option/", {
                        "startDate": this.state.startDate,
                        "endDate": this.state.endDate
                    }, {headers: {Authorization: `Bearer ${Auth.getAccessToken()}`}}).then(response => {
                        this.setState({
                            refreshPage: true
                        });
                    }).catch(err => {
                        console.log(err);
                    })
                })
            } else if (error.response && error.response.status === 403) {
                alert('you are not allowed to add option to this poll')
            } else if (error.response && error.response.status === 500) {
                alert('maybe your internet connection is poor')
            } else {
                console.log(error);
            }
        })
    };

    removeOption(optionId) {
        axios.delete('http://' + process.env.REACT_APP_BACKEND_HOST + ':' + process.env.REACT_APP_BACKEND_PORT + '/poll/option/${optionId}/', {
            headers: {Authorization: `Bearer ${Auth.getAccessToken()}`}
        }).then(response => {
            this.setState({
                refreshPage: true
            });
        }).catch(error => {
            if (error.response && error.response.data.code === "token_not_valid") {
                axios.post('http://' + process.env.REACT_APP_BACKEND_HOST + ':' + process.env.REACT_APP_BACKEND_PORT + '/auth/token/refresh/', {
                    refresh: Auth.getRefreshToken()
                }).then(response => {
                    Auth.authenticateUser(response.data.access, Auth.getRefreshToken());
                    axios.delete('http://' + process.env.REACT_APP_BACKEND_HOST + ':' + process.env.REACT_APP_BACKEND_PORT + '/poll/option/${optionId}/', {
                        headers: {Authorization: `Bearer ${Auth.getAccessToken()}`}
                    }).then(response => {
                        this.setState({
                            refreshPage: true
                        });
                    }).catch(err => {
                        console.log(err);
                    })
                })
            } else if (error.response && error.response.status === 403) {
                alert('you are not allowed to delete option from this poll')
            } else if (error.response && error.response.status === 500) {
                alert('maybe your internet is poor')
            } else {
                console.log(error);
            }
        })
    }

    removeInvitee(invitee) {
        let invitees = [];
        invitees.push(invitee.email);
        axios.delete('http://' + process.env.REACT_APP_BACKEND_HOST + ':' + process.env.REACT_APP_BACKEND_PORT + '/poll/${this.props.match.params.pollId}/participant/', {
            headers: {
                Authorization: `Bearer ${Auth.getAccessToken()}`
            }, data: {
                invitees: invitees
            }
        }).then(response => {
            this.setState({
                refreshPage: true
            });
        }).catch(error => {
            if (error.response && error.response.data.code === "token_not_valid") {
                axios.post('http://' + process.env.REACT_APP_BACKEND_HOST + ':' + process.env.REACT_APP_BACKEND_PORT + '/auth/token/refresh/', {
                    refresh: Auth.getRefreshToken()
                }).then(response => {
                    Auth.authenticateUser(response.data.access, Auth.getRefreshToken());
                    axios.delete('http://' + process.env.REACT_APP_BACKEND_HOST + ':' + process.env.REACT_APP_BACKEND_PORT + '/poll/${this.props.match.params.pollId}/participant/', {
                        headers: {
                            Authorization: `Bearer ${Auth.getAccessToken()}`
                        },
                        data: {
                            invitees: invitees
                        }
                    }).then(response => {
                        this.setState({
                            refreshPage: true
                        });
                    }).catch(err => {
                        console.log(err);
                    })
                })
            } else if (error.response && error.response.status === 403) {
                alert('you are not allowed to remove participant from this poll')
            } else if (error.response && error.response.status === 500) {
                alert('maybe your internet connection is poor')
            } else {
                console.log(error);
            }
        })
    }

    handleInviteesChange = i => e => {
        let invitees = [...this.state.addedInvitees];
        invitees[i] = e.target.value;
        this.setState({
            addedInvitees: invitees
        })
    };

    addInvitee = e => {
        e.preventDefault();
        let addedInvitees = this.state.addedInvitees.concat(['']);
        this.setState({
            addedInvitees
        })
    };

    addInviteeToPoll = () => {
        const {addedInvitees} = this.state;
        const errors = validate(addedInvitees);
        if (errors.email.length !== 0) {
            this.setState({errors});
        } else {
            this.setState({
                errors: {email: []}
            });
            axios.post("http://" + process.env.REACT_APP_BACKEND_HOST + ":" + process.env.REACT_APP_BACKEND_PORT + "/poll/" + this.props.match.params.pollId + "/participant/", {
                "invitees": addedInvitees
            }, {headers: {Authorization: `Bearer ${Auth.getAccessToken()}`}}).then(response => {
                this.setState({
                    refreshPage: true
                });
            }).catch(error => {
                if (error.response && error.response.data.code === "token_not_valid") {
                    axios.post('http://' + process.env.REACT_APP_BACKEND_HOST + ':' + process.env.REACT_APP_BACKEND_PORT + '/auth/token/refresh/', {
                        refresh: Auth.getRefreshToken()
                    }).then(response => {
                        Auth.authenticateUser(response.data.access, Auth.getRefreshToken());
                        axios.post("http://" + process.env.REACT_APP_BACKEND_HOST + ":" + process.env.REACT_APP_BACKEND_PORT + "/poll/" + this.props.match.params.pollId + "/participant/", {
                            "invitees": addedInvitees
                        }, {headers: {Authorization: `Bearer ${Auth.getAccessToken()}`}}).then(response => {
                            this.setState({
                                refreshPage: true
                            });
                        }).catch(err => {
                            console.log(err);
                        })
                    })
                } else if (error.response && error.response.status === 403) {
                    alert('you are not allowed to add invitee to this poll')
                } else if (error.response && error.response.status === 400) {
                    alert('at least one of the invitees is not registered in the jalas system')
                } else if (error.response && error.response.status === 500) {
                    alert('maybe your internet connection is poor')
                } else {
                    console.log(error);
                }
            })
        }
    };


        render() {
            if (!this.state.poll)
                return (<div></div>);

            if (this.state.redirect)
                return <Redirect to={"/meeting/" + this.state.meetingId}/>;

            if (this.state.refreshPage)
                window.location.reload();

            const {poll} = this.state;
            const {availableRooms} = this.state;
            const {selectedOption} = this.state;
            const {createMeeting} = this.state;
            const {startDate} = this.state;
            const newSet = new Set(this.state.errors.email);
            const emailErrors = [...newSet];

            const ShowAvailableRooms = () => {
                return (
                    <div>
                        {selectedOption && availableRooms && availableRooms !== 'Something Bad Happened in Getting Available Rooms' &&
                        <p className="solid">
                            <b className="show-available-rooms"> Available Rooms for
                                Option {selectedOption.index} : </b> {availableRooms}
                        </p>}
                        {selectedOption && availableRooms === 'Something Bad Happened in Getting Available Rooms' &&
                        <p className="solid"><b className="show-available-rooms">
                            {availableRooms} </b></p>}
                    </div>
                )
            };

            const inviteesJSX = poll.invitees.map(invitee => {
                return (
                    <div className="d-flex bd-highlight example-parent">
                        <div className="p-1 bd-highlight col-example">
                            <li>
                                <p><b> email : &emsp; </b> {invitee.email} </p>
                            </li>
                        </div>

                        <div className="p-1 bd-highlight col-example remove-participant">
                            <Button onClick={() => this.removeInvitee(invitee)}
                                    variant="outline-dark">
                                <b> Delete Invitee </b>
                            </Button>
                        </div>
                    </div>
                )
            });

            const optionsJSX = poll.options.map((option, index) => {
                return (
                    <div>
                        <p> {index + 1}: </p>
                        <p><b> start : &emsp; </b> {option.start} </p>
                        <p><b> end : &emsp; </b> {option.end} </p>
                        <p><b> Positive Votes : &emsp; </b> {option.positiveVotes} </p>
                        <p><b> Negative Votes : &emsp; </b> {option.negativeVotes} </p>
                        <p><b> Agree if Needed Votes : &emsp; </b> {option.ifAgreeVotes} </p>

                        <div>
                            <Button className="vote-button" onClick={() => this.handleVotes(option.id, 1)}
                                    variant="outline-dark">
                                <b> Vote + </b>
                            </Button>
                            <Button className="vote-button" onClick={() => this.handleVotes(option.id, -1)}
                                    variant="outline-dark">
                                <b> Vote - </b>
                            </Button>
                            <Button className="vote-button" onClick={() => this.handleVotes(option.id, 0)}
                                    variant="outline-dark">
                                <b> Agree if Needed </b>
                            </Button>
                        </div>

                        <div className="available-rooms-button">
                            <Button onClick={() => this.getAvailableRooms(option.id, index + 1)} variant="outline-dark">
                                <b> Show Available Rooms </b>
                            </Button>
                            <Button onClick={() => this.removeOption(option.id)}
                                    className="remove-option" variant="outline-dark">
                                <b> Remove Option </b>
                            </Button>
                        </div>

                        <br/><br/>
                    </div>
                )
            });


            return (
                <div id='poll-info-container'>
                    <Navbar bg="dark" variant="dark">
                        <Navbar.Text className="nav-location"><h1> Jalas System </h1></Navbar.Text>
                    </Navbar>

                    <div className="show-poll">
                        <p>
                            <b> Title : &emsp; </b> {poll.title}
                        </p>
                        <p>
                            <b> Creator : &emsp;</b> {poll.creator}
                        </p>
                        <p>
                            <b> Invitees : &emsp;</b>
                            <ul> {inviteesJSX} </ul>
                        </p>
                        <p>
                            <b> Options : &emsp;</b>
                            {optionsJSX}
                        </p>
                    </div>

                    <ShowAvailableRooms/>

                    <hr/>
                    <h2 className="comment-header">Comments</h2>
                    <div className="d-flex bd-highlight example-parent">
                        <div className="p-2 flex-fill bd-highlight col-example comment-box">
                            <div className="d-flex bd-highlight example-parent">
                                <div className="p-2 flex-fill bd-highlight col-example">
                                <textarea className="comment-textarea" name="comment" placeholder="Add comment here"
                                          value={this.state.comment} onChange={c => this.handleComment(c)}></textarea>
                                </div>
                                <div className="p-2 flex-fill bd-highlight col-example">
                                    <Button variant="outline-dark" className="post-comment" onClick={this.postComment}>
                                        <b>Submit</b></Button>
                                </div>
                            </div>
                        </div>

                        <div className="p-2 flex-fill bd-highlight col-example">
                            <Button variant="outline-dark"
                                    href={'/poll/' + this.props.match.params.pollId + '/comment/'}>
                                <b> View Comments </b>
                            </Button>
                        </div>
                    </div>
                    <hr/>

                    <h2 className="comment-header">Add Option</h2>
                    {startDate.map((option, index) => (
                        <div>
                            <div className="option-location rowC">
                                <p>Start</p>
                                <div className="datetime-location">
                                    <DatePicker
                                        selected={(this.state.startDate.length === 0) ? new Date() :
                                            (this.state.startDate[0] === "") ? new Date() : this.state.startDate[index]}
                                        onChange={(date) => this.handleStartOptionChange(index, date)}
                                        showTimeSelect
                                        timeFormat="HH:mm"
                                        timeIntervals={15}
                                        timeCaption="time"
                                        dateFormat="MMMM d, yyyy h:mm aa"
                                    />
                                </div>
                            </div>
                            <div className="option-location rowC">
                                <p>End</p>
                                <div className="datetime-location">
                                    <DatePicker
                                        selected={(this.state.endDate.length === 0) ? new Date() :
                                            (this.state.endDate[0] === "") ? new Date() : this.state.endDate[index]}
                                        onChange={(date) => this.handleEndOptionChange(index, date)}
                                        showTimeSelect
                                        timeFormat="HH:mm"
                                        timeIntervals={15}
                                        timeCaption="time"
                                        dateFormat="MMMM d, yyyy h:mm aa"
                                    />
                                </div>
                            </div>
                        </div>
                    ))}

                    <div>
                        <Button variant="outline-dark" className="addoption" onClick={this.addOption}>Add New
                            Option</Button>
                    </div>

                    <div>
                        <Button variant="outline-dark" className="addoption"
                                onClick={this.addOptionToPoll}>Save</Button>
                    </div>
                    <hr/>


                    <h2 className="comment-header">Add Invitee</h2>
                    {this.state.addedInvitees.map((invitee, index) => (
                        <div key={index}>
                            <Form.Group as={Row} controlId="formHorizontalEmail">
                                <Form.Label className="invitee-label-location-2">
                                    Invitee
                                </Form.Label>
                                <Form.Control className="invitee-placeholder-location-2" type="email"
                                              placeholder="Invitee"
                                              onChange={this.handleInviteesChange(index)}/>
                            </Form.Group>
                        </div>
                    ))}

                    {emailErrors.map(error => (
                        <p className="show-errors"><b> {error} </b></p>
                    ))}
                    <div>
                        <Button variant="outline-dark" className="addoption" onClick={this.addInvitee}>Add
                            New Invitee</Button>
                    </div>
                    <div>
                        <Button variant="outline-dark" className="addoption"
                                onClick={this.addInviteeToPoll}>Save</Button>
                    </div>
                    <hr/>


                    <div className="d-flex bd-highlight example-parent">
                        <div className="p-2 flex-fill bd-highlight col-example">
                            <Form.Group as={Row}>
                                <Form.Control type="number" className="room-number-input" placeholder="Room Number"
                                              onChange={e => this.updateRoomNumber(e)}/>
                            </Form.Group>
                        </div>
                        <div className="p-2 flex-fill bd-highlight col-example">
                            <Button variant="outline-dark" onClick={this.createMeeting}>Create Meeting</Button>
                        </div>
                        <div className="p-2 flex-fill bd-highlight col-example"></div>
                    </div>

                    <div>
                        {createMeeting && <p className="solid">
                            {createMeeting}
                        </p>}
                    </div>

                </div>
            )
        }
    }
