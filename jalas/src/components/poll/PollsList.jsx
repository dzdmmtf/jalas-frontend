import React, {Component} from 'react'
import '../home/Home.css'
import axios from 'axios';
import Navbar from "react-bootstrap/Navbar";
import {Button} from 'react-bootstrap';
import Auth from "../authentication/Auth";


export default class PollsList extends Component {

    componentDidMount() {
        let setState = this.setState.bind(this);
        
        axios.get('http://' + process.env.REACT_APP_BACKEND_HOST + ':' + process.env.REACT_APP_BACKEND_PORT + '/poll/',  { headers: { Authorization: `Bearer ${Auth.getAccessToken()}`}}).then(response => {
            setState({polls: response.data.polls});
        }).catch(error => {
            if (error.response && error.response.data.code === "token_not_valid") {
                axios.post('http://' + process.env.REACT_APP_BACKEND_HOST + ':' + process.env.REACT_APP_BACKEND_PORT + '/auth/token/refresh/', {
                    refresh: Auth.getRefreshToken()
                }).then(response => {
                    Auth.authenticateUser(response.data.access, Auth.getRefreshToken());
                    axios.get('http://' + process.env.REACT_APP_BACKEND_HOST + ':' + process.env.REACT_APP_BACKEND_PORT + '/poll/',  { headers: { Authorization: `Bearer ${Auth.getAccessToken()}`}}).then(response => {
                        setState({polls: response.data.polls});
                    }).catch(err => {
                        console.log(err);
                    })
                })
            } else if (error.response && error.response.status === 403) {
                alert('you are not allowed to see poll list')
            }  else {
                console.log(error);
            }
        })
    }

    render() {
        if (!this.state)
            return (<div></div>);
        const pollsJSX = this.state.polls.map(p => {
            const poll = JSON.parse(JSON.stringify(p));
            return (
                <div className="button-div">
                    <Button variant="outline-dark" href={"/poll/" + poll.id + "/"} key={poll.id} id={poll.id}>
                        <b> {poll.title} </b>
                    </Button>
                </div>
            )
        });

        return (
            <div>
                <Navbar bg="dark" variant="dark">
                    <Navbar.Text className="nav-location"><h1> Jalas System </h1></Navbar.Text>
                </Navbar>

                <Button variant="outline-dark" href="/poll/create/" className="create-poll">
                    <b> Create Poll </b>
                </Button>
                <div id='polls-container'>
                    {pollsJSX}
                </div>
            </div>
        )
    }
}
