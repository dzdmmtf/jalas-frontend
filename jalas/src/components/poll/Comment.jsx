import React, {Component} from 'react'
import {Redirect} from 'react-router'
import './Poll.css'
import axios from 'axios';
import Navbar from "react-bootstrap/Navbar";
import {Button} from 'react-bootstrap';
import Auth from "../authentication/Auth";

export default class Comment extends Component{
    componentDidMount() {
        let setState = this.setState.bind(this);

        axios.get('http://' + process.env.REACT_APP_BACKEND_HOST + ':' + process.env.REACT_APP_BACKEND_PORT + '/poll/' + this.props.match.params.pollId + '/comment/',
            { headers: { Authorization: `Bearer ${Auth.getAccessToken()}`}}).then(response => {
            setState({comments: response.data.comments});
        }).catch(error => {
            if (error.response && error.response.data.code === "token_not_valid") {
                axios.post('http://' + process.env.REACT_APP_BACKEND_HOST + ':' + process.env.REACT_APP_BACKEND_PORT + '/auth/token/refresh/', {
                    refresh: Auth.getRefreshToken()
                }).then(response => {
                    Auth.authenticateUser(response.data.access, Auth.getRefreshToken());
                    axios.get('http://' + process.env.REACT_APP_BACKEND_HOST + ':' + process.env.REACT_APP_BACKEND_PORT + '/poll/' + this.props.match.params.pollId + '/comment/',
                        { headers: { Authorization: `Bearer ${Auth.getAccessToken()}`}}).then(response => {
                        setState({comments: response.data.comments});
                    }).catch(err => {
                        console.log(err);
                    })
                })
            } else if (error.response && error.response.status === 403) {
                alert('you are not allowed to see comment list of this poll')
            }  else {
                console.log(error);
            }
        })
    }

    removeComment(comment) {
        axios.delete('http://' + process.env.REACT_APP_BACKEND_HOST + ':' + process.env.REACT_APP_BACKEND_PORT + '/poll/comment/${comment.id}/', {
            headers: {Authorization: `Bearer ${Auth.getAccessToken()}`}}).then(response => {
            this.setState({
                refreshPage: true
            });
        }).catch(error => {
            if (error.response && error.response.data.code === "token_not_valid") {
                axios.post('http://' + process.env.REACT_APP_BACKEND_HOST + ':' + process.env.REACT_APP_BACKEND_PORT + '/auth/token/refresh/', {
                    refresh: Auth.getRefreshToken()
                }).then(response => {
                    Auth.authenticateUser(response.data.access, Auth.getRefreshToken());
                    axios.delete('http://' + process.env.REACT_APP_BACKEND_HOST + ':' + process.env.REACT_APP_BACKEND_PORT + '/poll/comment/${comment.id}/', {
                        headers: {Authorization: `Bearer ${Auth.getAccessToken()}`}}).then(response => {
                        this.setState({
                            refreshPage: true
                        });
                    }).catch(err => {
                        console.log(err);
                    })
                })
            } else if (error.response && error.response.status === 403) {
                alert('you are not allowed to delete comment from this poll')
            } else {
                console.log(error);
            }
        })
    };

    removeAllComments = () => {
        axios.delete('http://' + process.env.REACT_APP_BACKEND_HOST + ':' + process.env.REACT_APP_BACKEND_PORT + '/poll/${this.props.match.params.pollId}/comment/', {
            headers: {Authorization: `Bearer ${Auth.getAccessToken()}`}}).then(response => {
            this.setState({
                refreshPage: true
            });
        }).catch(error => {
            if (error.response && error.response.data.code === "token_not_valid") {
                axios.post('http://' + process.env.REACT_APP_BACKEND_HOST + ':' + process.env.REACT_APP_BACKEND_PORT + '/auth/token/refresh/', {
                    refresh: Auth.getRefreshToken()
                }).then(response => {
                    Auth.authenticateUser(response.data.access, Auth.getRefreshToken());
                    axios.delete('http://' + process.env.REACT_APP_BACKEND_HOST + ':' + process.env.REACT_APP_BACKEND_PORT + '/poll/${this.props.match.params.pollId}/comment/', {
                        headers: {Authorization: `Bearer ${Auth.getAccessToken()}`}}).then(response => {
                        this.setState({
                            refreshPage: true
                        });
                    }).catch(err => {
                        console.log(err);
                    })
                })
            } else if (error.response && error.response.status === 403) {
                alert('you are not allowed to delete comment from this poll')
            } else {
                console.log(error);
            }
        })
    };

    render() {
        if (!this.state)
            return (<div> </div>);

        if (this.state.redirect)
            return <Redirect to={"/poll/" + this.props.match.params.pollId}/>;

        if (this.state.refreshPage)
            window.location.reload();

        const commentsJSX = this.state.comments.map(c => {
            const comment = JSON.parse(JSON.stringify(c));
            return (
                <div>
                    <div className="button-div">
                        <b> Author: </b> <p> {comment.username} </p>
                        <b> Message: </b> <p> {comment.text} </p>
                    </div>
                    <Button onClick={() => this.removeComment(comment)} variant="outline-dark" className="back-location">
                        <b> Delete </b>
                    </Button>
                    <hr/>
                </div>

            )
        });

        return (
            <div>
                <Navbar bg="dark" variant="dark">
                    <Navbar.Text><h1 className="nav-location"> Jalas System </h1></Navbar.Text>
                </Navbar>
                <div>
                    {commentsJSX}
                </div>
                <div>
                    <Button onClick={this.removeAllComments} variant="outline-dark" className="back-location">
                        <b> Delete All Comments </b>
                    </Button>
                    <Button onClick={() => {this.setState({redirect: true})}} variant="outline-dark" className="back-location">
                        <b> Back </b>
                    </Button>
                </div>
            </div>
        )
    }
}