import React, {Component} from "react";
import { Button, FormGroup, FormControl } from "react-bootstrap";
import "./Login.css";
import axios from "axios";
import Auth from './Auth'
import Navbar from "react-bootstrap/Navbar";
import {Redirect} from "react-router";

export default class Login extends Component {
    state = {
            email: "",
            password: ""
        }

    validateForm() {
        return this.state.email.length > 0 && this.state.password.length > 0;
    }

    handleEmail(e) {
        this.setState({
            email: e.target.value
        })
    }

    handlePassword(e) {
        this.setState({
            password: e.target.value
        })
    }

    handleSubmit = () => {
        const {email, password} = this.state;
        axios.post("http://" + process.env.REACT_APP_BACKEND_HOST + ":" + process.env.REACT_APP_BACKEND_PORT + "/auth/token/" , {
            "username": email,
            "password": password
        }).then(response => {
            Auth.authenticateUser(response.data.access, response.data.refresh);
            this.setState({
                redirect: true
            })
        }).catch(error => {
            if(error.response.status === 401) {
                this.setState({
                    message: 'wrong email or password',
                    wrongCredentials: true
                })
            } else {
                this.setState({
                    message: 'something bad happened'
                })
            }

        })
    };


    render() {
        if (!this.state)
            return (<div> </div>);

        if (this.state.redirect)
            return <Redirect to={"/"}/>;

        const ShowError = () => {
            return (
                <div>
                    {this.state.wrongCredentials && <p className="solid">
                        <b className="show-available-rooms">{this.state.message} </b>
                    </p>}
                    {!this.state.wrongCredentials && this.state.message && <p className="solid">
                        <b className="show-available-rooms">{this.state.message} </b>
                    </p>}
                </div>
            )
        };

        return (
            <div className="Login">
                <Navbar bg="dark" variant="dark">
                    <Navbar.Text className="nav-location"><h1> Jalas System </h1></Navbar.Text>
                </Navbar>

                <form>
                    <FormGroup controlId="email" bsSize="large">
                        <FormControl
                            type="email"
                            value={this.state.email}
                            onChange={e => this.handleEmail(e)}
                        />
                    </FormGroup>
                    <FormGroup controlId="password" bsSize="large">
                        <FormControl
                            value={this.state.password}
                            onChange={e => this.handlePassword(e)}
                            type="password"
                        />
                    </FormGroup>

                    <Button variant="outline-dark" block bsSize="large" disabled={!this.validateForm()} onClick={this.handleSubmit}>
                        Login
                    </Button>

                    <ShowError/>
                </form>
            </div>
        );
    }
}