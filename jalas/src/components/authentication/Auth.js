class Auth {

    static authenticateUser(access, refresh) {
        localStorage.setItem('access', access);
        localStorage.setItem('refresh', refresh)
    }

    static isUserAuthenticated() {
        return localStorage.getItem('access') !== null;
    }

    static getAccessToken() {
        return localStorage.getItem('access');
    }

    static getRefreshToken() {
        return localStorage.getItem('refresh')
    }

}

export default Auth;